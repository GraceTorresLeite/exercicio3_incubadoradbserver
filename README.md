# Exercicio3_IncubadoraDBServer

## Revisão e Exercícios 

### Exercícios 

**8) Confira se completaste as guias discutidas até o momento:**

- [x] √ Building Java Projects with Maven
https://spring.io/guides/gs/maven/ ✔

https://gitlab.com/GraceTorresLeite/Incubadora_aula2.git
- [x] √ Building an Application with Spring Boot 
https://spring.io/guides/gs/spring-boot/ ✔

https://gitlab.com/GraceTorresLeite/incubadora_aula3.git
- [x] √ Serving Web Content with Spring MVC 
https://spring.io/guides/gs/serving-web-content/ ✔

https://gitlab.com/GraceTorresLeite/incubadora_aula4.git
- [x] √ Handling Form Submission 
https://spring.io/guides/gs/handling-form-submission/ ✔

https://gitlab.com/GraceTorresLeite/incubadora_aula5.git


**9) Complete as guias a seguir:**

- [x] Validating Form Input https://spring.io/guides/gs/validating-form-input/ ✔

https://gitlab.com/GraceTorresLeite/incubadora_aula6.git

- [x] Testing the Web Layer https://spring.io/guides/gs/testing-web/ ✔

 https://gitlab.com/GraceTorresLeite/incubadora_aula7.git

- [x] Securing a Web Application https://spring.io/guides/gs/securing-web/ ✔

https://gitlab.com/GraceTorresLeite/incubadora_aula8.git

- [x] 10) [EXTRA] Ative a Integração Contínua da PetClinic no GitLab. ✔
- [x] 11) [EXTRA] Acrescente um campo **"categoria"** na classe **PetType** para indicar que o tipo é **"doméstico" ou "exótico"**. Atualize a aplicação. ✔
- [x] 12) [EXTRA] Acrescente um campo **"modo"** na classe **Specialty** para indicar que a especialidade é **"hospital", "clínica" ou "internação"**. Atualize a aplicação. ✔
- [x] 13) [EXTRA] Copie e cole a tela da aplicação ou mensagem de erro relacionada com cada "issue". Basta capturar a tela e colar no comentário do "issue". ✔


